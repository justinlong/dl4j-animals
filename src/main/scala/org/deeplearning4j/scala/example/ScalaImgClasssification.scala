package org.deeplearning4j.scala.example

import java.io.{DataOutputStream, File, FileOutputStream, IOException}
import java.util.Random

import org.apache.commons.io.{FileUtils, FilenameUtils}
import org.canova.api.records.reader.RecordReader
import org.canova.api.split.LimitFileSplit
import org.canova.image.loader.BaseImageLoader
import org.canova.image.recordreader.ImageRecordReader
import org.deeplearning4j.datasets.canova.RecordReaderDataSetIterator
import org.deeplearning4j.datasets.iterator.DataSetIterator
import org.deeplearning4j.eval.Evaluation
import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.{GradientNormalization, MultiLayerConfiguration, NeuralNetConfiguration, Updater}
import org.deeplearning4j.nn.conf.layers.{ConvolutionLayer, DenseLayer, LocalResponseNormalization, OutputLayer, SubsamplingLayer}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.{DataSet, SplitTestAndTrain}
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.lossfunctions.LossFunctions

import scala.collection.mutable.ListBuffer
import collection.JavaConversions._

object ScalaImgClasssification {

  // you may optionally adjust any values here
  // please ensure your parameters are defined here
  // and use them inside the main method
  val seed = 123
  val height = 50
  val width = 50
  val channels = 3
  val numExamples = 80
  val outputNum = 4
  val batchSize = 20
  val listenerFreq = 5
  val appendLabels = true
  val iterations = 2
  val epochs = 2
  val splitTrainNum = 10


  def main(args: Array[String]) {
    val basePath = FilenameUtils.concat(System.getProperty("user.dir"), "src/main/resources/")
    val mainPath: File = new File(basePath, "animals")
    val labels: List[String] = List("bear", "deer", "duck", "turtle")

    /*
      Below you will need to implement your image loading functionality using the given dataset.
     */

    /*
      Implement your neural network configuration using DL4J's MultiLayer.
     */

    /*
      Set any listeners necessary for the experiment such as ScoreIterationListener or HistogramListener.
     */

    /*
      Perform training logic here. Make sure you test using a test dataset.
     */

    /*
      Finally, serialize the model and save it to the local filesystem.
     */
    val confPath = FilenameUtils.concat(basePath, "AnimalModel-conf.json")
    val paramPath = FilenameUtils.concat(basePath, "AnimalModel-params.bin")
  }
}

